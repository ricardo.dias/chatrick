FactoryBot.define do
  factory :channel do
    slug { FFaker::Lorem.word }
    team
    user { Team.user }
  end
end
